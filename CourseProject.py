HANGMAN_PHOTOS = {1:"    x-------x",2:"    x-------x\n    |\n    |\n    |\n    |\n    |",3:"    x-------x\n    |       |\n    |       0\n    |\n    |\n    |",4:"    x-------x\n    |       |\n    |       0\n    |       |\n    |\n    |",5:"    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |\n    |",6:"    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |      /\n    |",7:"    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |      / \\\n    |"}
MAX_TRIES = 8

def check_valid_input(letter_guessed, old_letters_guessed):
    '''
    function checks if the user input is a single letter in english alphabet and not guessed before
    :param letter_guessed: user input
    :type letter_guessed: str
    :param old_letters_guessed: list of letters already guessed
    :type old_letters_guessed: list
    :return: bool
    '''
    if len(letter_guessed) > 1:
        return False
    elif letter_guessed.isalpha() == False:
        return False
    elif letter_guessed in old_letters_guessed:
        return False
    else:
        return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    '''
    function updates the list of letters already guessed
    :param letter_guessed: user input
    :type letter_guessed: str
    :param old_letters_guessed: list of letters already guessed
    :type old_letters_guessed: list
    :return: list
    '''
    letter_guessed = letter_guessed.lower()
    if check_valid_input(letter_guessed, old_letters_guessed) == True and not letter_guessed in old_letters_guessed:
        old_letters_guessed.append(letter_guessed)
        return True
    else:
        print("X")
        #print a sorted list of letters already guessed seperated by a "->"
        print(" -> ".join(sorted(old_letters_guessed)))
        return False

def check_win(secret_word,letters_guessed):
    '''
    function checks if the user won
    :param secret_word: secret word
    :type secret_word: str
    :param letters_guessed: list of letters already guessed
    :type letters_guessed: list
    :return: bool
    '''
    for letter in secret_word:
        if letter not in letters_guessed:
            return False
    return True
def print_hangman(num_of_tries):
    print(HANGMAN_PHOTOS[num_of_tries])

def show_hidden_word(secret_word, old_letters_guessed):
    '''
    function prints the hidden word
    :param secret_word: secret word
    :type secret_word: str
    :param old_letters_guessed: list of letters already guessed
    :type old_letters_guessed: list
    :return: String
    '''
    hidden_word = ""
    for letter in secret_word:
        if letter in old_letters_guessed:
            hidden_word += letter+" "
        else:
            hidden_word = hidden_word+ "_ " 
    return hidden_word

def choose_word(file_path, index):
    file_path = open(file_path, 'r')
    read_file = file_path.read()
    words = read_file.split()
    words = " ".join(sorted(set(words), key = words.index))
    read_file = list(read_file.split(" "))
    length = len(read_file)
    index = index - 1
    if length == 1:
        index = 0
    if index > length:
        index = abs(length - index)
    return read_file[index]


def welcome_screen():
    print("  _    _                                         \n | |  | |                                        \n | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  \n |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ \n | |  | | (_| | | | | (_| | | | | | | (_| | | | |\n |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|\n                      __/ |                      \n                     |___/")
    print(MAX_TRIES)
if __name__ == '__main__':
    welcome_screen()
    file_path = input("Enter file path: ")
    index = int(input("Enter index: "))
    print("Let's start")
    secret_word = choose_word(file_path, index)
    letters_guessed = []
    num_of_tries = 1
    while num_of_tries < MAX_TRIES:
        print_hangman(num_of_tries)
        print(show_hidden_word(secret_word, letters_guessed))
        letter_guessed = input("Enter a letter: ")
        if try_update_letter_guessed(letter_guessed, letters_guessed):
            num_of_tries += 1
            if check_win(secret_word, letters_guessed) == True:
                print(secret_word)
                print("WIN")
                break
    else:
        print(show_hidden_word(secret_word, letters_guessed))
        print("LOSE")
        
        


