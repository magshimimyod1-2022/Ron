
import socket
import datetime


SERVER_IP = "34.218.16.79"
SERVER_PORT = 77


def calculate_checksum(date, city):
    """
    calculate_checksum 
    """
    count_date = 0
    calculated_city = city.split(",")[0]
    calculated_city = calculated_city.lower()
    gem_city = [ord(char) - 96 for char in calculated_city]
    gematria_city = 0
    for num in gem_city:
        gematria_city += (num)
    date = date.replace("/", "")
    for num in date:
        count_date += int(num)
    str_date = str(count_date)
    str_city = str(gematria_city)
    checksum = str_city+"."+str_date
    return checksum


def get_today_weather(city):
    '''
    today's weather
    '''
    proccessed_city = city.replace(" ", "")
    today = datetime.datetime.now()
    today = today.strftime("%d/%m/%Y")
    today_weather = get_weather(proccessed_city, today)
    return today_weather


def get_3_days_ahead_weather(city):
    """
    retrun a list of 3 days of weather forcast
    """
    weather_list = []
    today = datetime.datetime.now()
    today = today.strftime("%d/%m/%Y")
    weather_list.append(get_today_weather(city))
    for i in range(3):
        date = today
        date = datetime.datetime.strptime(date, "%d/%m/%Y")
        date = date + datetime.timedelta(days=i+1)
        date = date.strftime("%d/%m/%Y")
        weather_list.append(get_weather(city, date))
        print('{}, Temperature: {}, {}'.format(
            date, weather_list[i][0], weather_list[i][1]))


def get_request_string(city, date):
    """
    get request string
    """
    request = "100:REQUEST:city={}&date={}&checksum=".format(city, date)
    checksum = calculate_checksum(date, city)
    request += str(checksum)
    return request


def get_weather(city, date):
    """
    extract the city name from the string and 
    get temperature and the weather description  using with method
    """

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((SERVER_IP, SERVER_PORT))
        request = get_request_string(city, date)

        s.sendall(request.encode())
        data = s.recv(1024)
        answer = data.decode()
        data = s.recv(1024)
        answer += data.decode()
        valid_code = '200' in answer

        if valid_code:
            # find temp and weather description
            # string format =  200:OK:city=Paris,date=29/03/2022,temp=16.5,weather=Few Clouds.
            temp_index = answer.find("temp=")
            weather_index = answer.find("text=")
            temp = answer[temp_index+5:weather_index-1]
            weather = answer[weather_index+5:]
            return temp, weather
        else:
            return 999, "Unknown City"


def main():
    """
    menu
    """
    print("1. Today's weather")
    print("2. 3 days ahead weather")
    choice = input("Enter your choice: ")
    if choice == "1":
        city = input("Enter city name: ")
        today_weather = get_today_weather(city)
        print("Today's weather: {}".format(today_weather))
    elif choice == "2":
        city = input("Enter city name: ")
        print("\n")
        get_3_days_ahead_weather(city)


if __name__ == '__main__':
    main()
