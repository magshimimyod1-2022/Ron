'''
connect to server
'''
import socket

HOST = '127.0.0.1'
PORT = 65432

requests = ['100:REQUEST:command=get_album_list&param=', '200:REQUEST:command=get_album_songs&param=', '300:REQUEST:command=get_song_length&param=',
    '400:REQUEST:command=get_song_lyrics&param=', '500:REQUEST:command=get_song_album&param=', '600:REQUEST:command=find_song_name&param=', '700:REQUEST:command=find_song_by_lyrics&param=','800:REQUEST:command=close_connection&param=']


def user_menu():
    print("\n")
    print('1. Get Album List')
    print('2. Get Song List')
    print('3. Get Song Length')
    print('4. Get Song Lyrics')
    print('5. Get Song Album')
    print('6. Find Song Name')
    print('7. Find Song By Lyrics')
    print('8. Exit')
    return int(input('Enter your choice: '))


def handle_user_input(user_input):
    while True:
        if user_input == 1:
            print('Get Album List')
            return requests[user_input-1]
        elif user_input == 2:
            print('Get Song List')
            album_name = input('Enter album name: ')
            return requests[user_input-1] + album_name
        elif user_input == 3:
            print('Get Song Length')
            song_name = input('Enter song name: ')
            return requests[user_input-1] + song_name
        elif user_input == 4:
            print('Get Song Lyrics')
            song_name = input('Enter song name: ')
            return requests[user_input-1] + song_name
        elif user_input == 5:
            print('Get Song Album')
            song_name = input('Enter song name: ')
            return requests[user_input-1] + song_name
        elif user_input == 6:
            print('Find Song Name')
            song_name = input('Enter song name: ')
            return requests[user_input-1] + song_name
        elif user_input == 7:
            print('Find Song By Lyrics')
            lyrics = input('Enter song lyrics: ')
            return requests[user_input-1] + lyrics
        elif user_input == 8:
            print('Exit')
            break
        else:
            print('Invalid input')
        user_input = user_menu()
    return requests[user_input-1]



def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        while True:

            data = s.recv(1024)
            print('Received', data.decode())
            user_input = user_menu()
            s.sendall(handle_user_input(user_input).encode())
            if user_input == 8:
                break


if __name__ == '__main__':
    main()
